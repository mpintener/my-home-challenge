# MyHomeChallenge

# About this project

The application is divided in modules. Every module has their own routes. I think this is the best way because you can handle big projects.

PROPERTY MODULE:
Displays a list of properties and their features.
I could see that some main photos or agent logos were not found, so I handle this error uploading a default img instead.
When I was implementing the image gallery, I found out that there were some images with the same error (404 not found), so I filtered those images, and only show the available ones.

CHART MODULE:
The task said I couldn't use a 3rd party library to build the component, and by that I understood I cannot use a chart library. So after some research, I decided to use a library called d3, which is used for data visualization and many chart libraries use it to render their charts.




This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
