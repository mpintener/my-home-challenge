import { trigger, style, animate, transition, query, stagger } from '@angular/animations';

export const fadeInFadeOutAnimation = trigger(
  'fadeInFadeOutAnimation', [
    transition(':enter', [
      style({ opacity: 0 }),
      animate('600ms', style({ opacity: 1 }))
    ]),
    transition(':leave', [
      style({ opacity: 1 }),
      animate('600ms', style({ opacity: 0 }))
    ])
  ]
);

export const listAnimation = trigger('listAnimation', [
  transition('* => *', [ // each time the binding value changes
    query(':enter', [
      style({ opacity: 0 }),
      stagger(100, [
        animate('0.5s', style({ opacity: 1 }))
      ]),
    ], { optional: true }),

  ])
]);
