import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/property'
  },
  {
    path: '',
    children: [
      {
        path: 'property',
        loadChildren: './property/property.module#PropertyModule'
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'chart',
        loadChildren: './chart/chart.module#ChartModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/property'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
