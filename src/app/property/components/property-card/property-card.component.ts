import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Property } from 'src/app/model/property';
import { Router } from '@angular/router';
import { GALLERY_IMAGE, NgxImageGalleryComponent, GALLERY_CONF } from 'ngx-image-gallery';

@Component({
  selector: 'app-property-card',
  templateUrl: './property-card.component.html',
  styleUrls: ['./property-card.component.scss']
})
export class PropertyCardComponent implements OnInit {
  @Input() property: Property;
  images: GALLERY_IMAGE[];
  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false
  };

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.images = this.property.photos.map(photo => {
      return {
        url: photo,
        altText: 'Property image',
        title: 'Property image',
        thumbnailUrl: photo
      };
    });
    this.searchForBrokenUrls();
  }

  searchForBrokenUrls(): any {
    // Check if url is broken or not found and delete it from array
    this.images.forEach((img, index) => {
      const image = new Image();
      image.src = this.images[index].url;
      image.onerror = (error) => {
        this.images = this.images.filter((img, i) => i !== index);
      };
    });
  }

  handleGroupImgError() {
    this.property.groupLogo = './assets/img/isologo.jpg';
  }

  handleMainPhotoError() {
    this.property.mainPhoto = './assets/img/no-img.jpg';
  }

  showBrochure() {
    window.open(`/property/brochure/${this.property.id}`, '_blank');
  }

  openGallery(index: number = 0) {
    this.ngxImageGallery.open(index);
  }
}
