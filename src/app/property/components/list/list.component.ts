import { Component, OnInit } from '@angular/core';
import { Property } from 'src/app/model/property';
import { FormGroup, FormBuilder } from '@angular/forms';
import { fadeInFadeOutAnimation, listAnimation } from 'src/app/shared/animations/animations';

declare var require: any;
const propertiesJson: any = require('../../../shared/data/property.json');

export enum SortTypes {
  'asc',
  'desc',
  'default'
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [fadeInFadeOutAnimation, listAnimation]
})
export class ListComponent implements OnInit {
  properties: Property[];
  searchPropertyForm: FormGroup;
  iconButton: string;
  sortType: SortTypes;

  constructor(private fb: FormBuilder) {
    this.searchPropertyForm = this.fb.group({
      search: [null]
    });
    this.sortType = SortTypes['default'];
  }

  ngOnInit() {
    this.setPropertiesArray();
  }

  sortListByPrice(sortAgain = true) {
    if (this.sortType === SortTypes['asc']) {
      if (sortAgain) {
        this.sortDesc();
      } else {
        this.properties.sort((p1, p2) => p1.price - p2.price);
      }
    } else if (this.sortType === SortTypes['desc']) {
      if (sortAgain) {
        this.sortAsc();
      } else {
        this.properties.sort((p1, p2) => p2.price - p1.price);
      }
    } else if (this.sortType === SortTypes['default']) {
      if (sortAgain) {
        this.sortAsc();
      } else {
        this.setPropertiesArray();
      }
    }
  }

  sortAsc() {
    this.sortType = SortTypes['asc'];
    this.properties.sort((p1, p2) => p1.price - p2.price);
    this.iconButton = 'fa fa-chevron-up';
  }

  sortDesc() {
    this.sortType = SortTypes['desc'];
    this.properties.sort((p1, p2) => p2.price - p1.price);
    this.iconButton = 'fa fa-chevron-down';
  }

  onSubmit() {
    const formValue = this.searchPropertyForm.value;
    if (formValue.search !== null) {
      this.setPropertiesArray();
      this.properties = this.properties.filter(property => property.price === formValue.search);
    } else {
      this.setPropertiesArray();
      if (this.sortType !== SortTypes['default']) {
        this.sortListByPrice(false);
      }
    }
  }

  setPropertiesArray() {
    this.properties = propertiesJson['SearchResults'].map(p => new Property(p));
  }
}

