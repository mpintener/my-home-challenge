import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Property } from 'src/app/model/property';
import { fadeInFadeOutAnimation } from 'src/app/shared/animations/animations';

declare var require: any;
const propertiesJson: any = require('../../../shared/data/property.json');

@Component({
  selector: 'app-brochure',
  templateUrl: './brochure.component.html',
  styleUrls: ['./brochure.component.scss'],
  animations: [fadeInFadeOutAnimation]
})
export class BrochureComponent implements OnInit {
  idProperty: string;
  property: Property;

  constructor(private route: ActivatedRoute) {
    this.idProperty = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.property = propertiesJson['SearchResults'].find(pr => pr.PropertyId === Number(this.idProperty));
  }

}
