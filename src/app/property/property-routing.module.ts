import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './components/list/list.component';
import { BrochureComponent } from './components/brochure/brochure.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: 'Properties list'
        }
      },
      {
        path: 'brochure/:id',
        component: BrochureComponent,
        data: {
          title: 'Property brochure'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyRoutingModule { }
