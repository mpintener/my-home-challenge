import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyRoutingModule } from './property-routing.module';
import { ListComponent } from './components/list/list.component';
import { PropertyCardComponent } from './components/property-card/property-card.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { BrochureComponent } from './components/brochure/brochure.component';
import { NgxImageGalleryModule } from 'ngx-image-gallery';

@NgModule({
  imports: [
    CommonModule,
    PropertyRoutingModule,
    ReactiveFormsModule,
    CurrencyMaskModule,
    NgxImageGalleryModule
  ],
  declarations: [
    BrochureComponent,
    ListComponent,
    PropertyCardComponent
  ]
})
export class PropertyModule { }
