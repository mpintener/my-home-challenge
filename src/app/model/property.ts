export class Property {
  id?: number;
  groupLogo: string;
  bedsString: string;
  bathsString?: string;
  price: any;
  priceString: any;
  sizeStringMeters?: string;
  displayAddress: string;
  propertyType: string;
  berRating?: string;
  mainPhoto: string;
  photos: string[];

  constructor(property) {
    if (property.PropertyId) { this.id = property.PropertyId; }
    this.groupLogo = property.GroupLogoUrl;
    this.bedsString = property.BedsString;
    if (property.BathString) { this.bathsString = property.BathString; }
    if (typeof property.Price === 'number') {
      this.priceString = `€ ${(property.Price).toLocaleString('en-us')}`;
    } else {
      this.priceString = property.Price;
    }
    this.price = property.Price;
    if (property.sizeStringMeters) { this.sizeStringMeters = property.SizeStringMeters; }
    this.displayAddress = property.DisplayAddress;
    this.propertyType = property.PropertyType;
    if (property.berRating) { this.berRating = property.BerRating; }
    this.mainPhoto = property.MainPhoto;
    this.photos = property.Photos;
  }
}
