import { Component, OnInit } from '@angular/core';
import { fadeInFadeOutAnimation } from 'src/app/shared/animations/animations';

declare var require: any;
const chartDataJson: any = require('../../../shared/data/bar-chart.json');

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  animations: [fadeInFadeOutAnimation]
})

export class ViewComponent implements OnInit {
  chartData: any[];
  graphHeight: number;
  graphWidth: number;

  constructor() {
    this.graphHeight = 300;
    this.graphWidth = 600;
  }

  ngOnInit() {
    this.chartData = chartDataJson;
  }


}
