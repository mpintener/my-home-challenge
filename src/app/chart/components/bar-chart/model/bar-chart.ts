export interface BarChartModel {
  label: string;
  percentage: number;
  amount: number;
}
