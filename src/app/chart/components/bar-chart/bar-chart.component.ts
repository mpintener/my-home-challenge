import { Component, Input, OnInit } from '@angular/core';
import { BarChartModel } from './model/bar-chart';
import { listAnimation } from 'src/app/shared/animations/animations';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
  animations: [listAnimation]
})
export class BarChartComponent implements OnInit {
  @Input() chartData: BarChartModel[];
  @Input() graphHeight: number;
  @Input() graphWidth: number;
  barsWidth: number;
  barsWidthString: string;
  containerWidth: string;
  graphHeightString: string;
  graphWidthString: string;
  yScale: string[];
  yScaleNumber: number[];

  constructor() {
    this.graphHeight = 253;
    this.graphWidth = 380;
    this.barsWidth = 45;
  }

  ngOnInit() {
    this.graphHeightString = `${this.graphHeight}px`;
    this.barsWidthString = `${this.barsWidth}px`;
    this.graphWidthString = `${this.graphWidth}px`;
    this.setYAxis();
    this.setContainersWidth();
  }

  setYAxis(): void {
    const maxValueFound: number = this.chartData.reduce((max, b) => Math.max(max, b.percentage), this.chartData[0].percentage);
    const yScale = [0];
    let num = 0;
    do {
      num += 10;
      yScale.push(num);
    } while (num < maxValueFound);
    this.yScaleNumber = yScale;
    this.yScale = yScale.sort((n1, n2) => n2 - n1).map(value => `${value}%`);
  }

  getBarHeight(percentage: number): string {
    const maxScaleValue: number = this.yScaleNumber.reduce((max, b) => Math.max(max, b), this.yScaleNumber[0]);
    const barHeight = (percentage * this.graphHeight) / maxScaleValue;
    return `${barHeight}px`;
  }

  getYPosition(): string {
    return `${this.graphHeight / (this.yScaleNumber.length - 1)}px`;
  }

  setContainersWidth() {
    const width = this.graphWidth / this.chartData.length;
    this.containerWidth = `${width}px`;
  }
}
