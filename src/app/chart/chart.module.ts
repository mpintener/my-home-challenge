import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartRoutingModule } from './chart-routing.module';
import { ViewComponent } from './components/view/view.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ChartRoutingModule,
    NgbTooltipModule
  ],
  declarations: [
    BarChartComponent,
    ViewComponent
  ]
})
export class ChartModule { }
